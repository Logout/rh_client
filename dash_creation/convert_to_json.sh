#!/bin/bash
set -eu

output_file="consolidated.json"

echo "[" > "$output_file"

i=0
IFS=$'\n'
while read -r title; do
  folder=Dash${i}
  dash_file="${folder}/dash.mpd"

  if [ -f "$dash_file" ]; then
    duration=$(grep -oP '(?<=mediaPresentationDuration="PT)(([0-9]+H)?([0-9]+M)?([0-9]+\.[0-9]+S)?)' "$dash_file")

    duration_in_s=0
    if [[ $duration == *H* ]]; then
      hours=$(echo "$duration" | grep -oP '([0-9]+)H' | sed 's/H//')
      duration_in_s=$(echo "$duration_in_s + $hours * 3600" | bc)
    fi
    if [[ $duration == *M* ]]; then
      minutes=$(echo "$duration" | grep -oP '([0-9]+)M' | sed 's/M//')
      duration_in_s=$(echo "$duration_in_s + $minutes * 60" | bc)
    fi
    if [[ $duration == *S* ]]; then
      seconds=$(echo "$duration" | grep -oP '([0-9]+\.[0-9]+)S' | sed 's/S//')
      duration_in_s=$(echo "$duration_in_s + $seconds" | bc)
    fi
    duration_in_s=$(echo "import math;print(math.ceil($duration_in_s))" | python)

    echo "$(jq -n \
      --arg title "$title" \
      --arg file_name "RH/$folder/dash.mpd" \
      --arg duration "$duration_in_s" \
      '{title: $title, file_name: $file_name, duration: $duration|tonumber}')," >> "$output_file"
  fi
  (( i = i + 1 ))
done < titles.txt

# Remove the trailing comma from the last line of the JSON array
sed -i '$ s/.$//' "$output_file"

echo "]" >> "$output_file"

