export interface VideoStream {
  current_file: string,
  current_title: string,
  duration: number,
  current_time_in_s: number,
  next_file: string,
}
