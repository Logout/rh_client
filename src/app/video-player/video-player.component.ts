import { AfterContentInit, Component, inject } from '@angular/core';
import { StreamingService } from '../streaming.service';
import { VideoStream } from '../video-stream';

declare var dashjs: any;

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements AfterContentInit {
  streamingService: StreamingService = inject(StreamingService);
  videoStream: VideoStream | undefined;
  player = dashjs.MediaPlayer().create();

  constructor() {
  }

  ngAfterContentInit(): void {
    this.updateStream();
  }

  updateStream(): void {
    this.streamingService.getCurrentFile().then((videoStream) => {
      this.videoStream = videoStream;
      this.setPlayer();
    });
  }

  setPlayer(): void {
    let url = this.createUrl();

    let video = <HTMLVideoElement>document.querySelector("#videoPlayer");
    let videoTitle = document.querySelector("#videoTitle");

    if (videoTitle)
      videoTitle.textContent = this.videoStream?.current_title ?? "";
    this.player.setAutoPlay(true);
    this.player.on('streamInitialized', () => this.player.seek(this.videoStream?.current_time_in_s ?? 0))
    this.player.on('playbackEnded', () => {
      this.updateStream();
    })
    this.player.initialize(video, url.toString(), true);
  }

  createUrl(): URL {
    let url = new URL(this.videoStream?.current_file ?? '', window.location.origin);
    return url;
  }
}
